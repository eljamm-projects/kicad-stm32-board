# Credits
A huge thanks to [Phil's Lab](https://www.youtube.com/@PhilsLab) for his awesome [course](https://www.youtube.com/watch?v=aVUqaB0IMh4). I really recommend anyone who wants to learn PCB Design to follow it and to check out Phil's channel for other useful material.

# Screenshots
<p align="center">
	<img src="images/STM32-3D-front-cropped.png" alt="Front board" width="45%"/>
	<img src="images/STM32-3D-back-cropped.png" alt="Back board" width="45%"/>
	<img src="images/schematic.png" alt="Schematic" width="80%"/>
</p>
